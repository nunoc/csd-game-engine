# The Game Engine

The game engine is a simple machine. It may start a game or compute a guess, and
it will always return a `game_status`:

```
{
  status: RUNNING,
  word: 'banana',
  guesses: ['a', 'b', 'c', 'd'],
  display_word: 'b a _ a _ a',
  lives: 3 // Initial 5 - 2 wrong guesses
}
```

# 💡 Exercise 1 💡

Add the package you just created to your `package.json` file and try it! Well,
if you did everything right, a strange error will appear.

Fix this error (you might need to change the **random_line** package), and
update your `package.json` if needed.
