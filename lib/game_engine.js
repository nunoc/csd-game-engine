const random_line = require("@nunosantos/random-line");

const startGame = () => {
  let word = random_line.getRandomWord ().trim ();
  return {
    status: "RUNNING",
    word: word.trim ().toLowerCase (),
    lives: 6,
    display_word: word.replace (/./ig, "_ ").trim (),
    guesses: []
  };
};

function buildDisplayWord(guesses, word){
  let displayWord = '';
  for (let index = 0; index < word.length; index++) {
    let char = word[index];
    if(guesses.indexOf(char) !== -1){
      displayWord = displayWord + char + ' ';
    } else {
      displayWord = displayWord + '_ ';
    }
  }

  return displayWord.trim();
}

function computeStatus(displayWord, lives){
  let status = "RUNNING";

  if(displayWord.indexOf('_') == -1){
    status = "WIN";
  } else if (lives < 0){
    status = "DEAD";
  }

  return status;
}

function computeGuesses (guesses, guess) {
  if (guesses.indexOf(guess)==-1){
    guesses.push (guess);
  }
}

const takeGuess = (game_state, guess) => {
  guess = (guess || "").toLowerCase ()
  let guesses = [...game_state.guesses];
  computeGuesses (guesses, guess);

  // calculate rightGuess
  let rightGuess = guess.match(/[a-z]/) && game_state.word.indexOf (guess) != -1;

  let lives = rightGuess ?  game_state.lives : game_state.lives - 1;
  let displayWord = buildDisplayWord(guesses, game_state.word);
  //let success = computeSuccess (rightGuess, displayWord);
  let status = computeStatus(displayWord, lives);
  
  return {
    status: status,
    word: game_state.word,
    lives: lives,
    display_word: displayWord,
    guesses: guesses
  };
};

module.exports = {
  startGame,
  takeGuess
};
